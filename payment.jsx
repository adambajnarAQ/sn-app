import React from 'react'
import Configurator from '../../configurator'
import { PayPalButton } from 'react-paypal-button-v2'
import {
    i18n,
    withRouter,
} from '../../validator'
import loadScriptAsync from 'async-scripts-loader'
import Loader from '../../front/components/shared/loader'
import { PAYPAL_URL } from '../../consts'

@i18n()
@withRouter()
export default class SFNPayment extends Configurator {

    state = {
        busy: true,
    }

    componentDidMount() {
        loadScriptAsync([PAYPAL_URL])
            .then(() => this.setState({ busy: false }))
    }

    render() {
        const { busy } = this.state

        const {
            t,
            history,
        } = this.props

        if (busy) {
            return (
                <div className="pt-5 col-12 col-lg-7 mx-auto">
                    <Loader />
                </div>
            )
        }

        return (
            <div className="pt-5 col-12 col-lg-7 mx-auto text-center">
                <div
                    onClick={() => history.push('/')}
                    className="my-3 cursor-pointer text-left"
                >
                    <strong>&lt;&nbsp;{t('sfn.backButton.text')}</strong>
                </div>
                <h1>{t('sfn.payment.title')}</h1>
                <p className="pb-4">{t('sfn.payment.subTitle')}</p>
                <PayPalButton
                    amount="1"
                    style={{
                        shape: 'pill',
                        color: 'silver',
                        layout: 'vertical',
                        label: 'buynow',
                    }}
                    createOrder={(data, actions) => (
                        actions.order.create({
                            purchase_units: [{
                                amount: {
                                    currency_code: 'USD',
                                    value: '1',
                                },
                            }],
                        })
                    )}
                    onSuccess={details => {
                        window.swalert(t('sfn.payment.successMessage.1') + details.payer.name.given_name + '!'
                            + '\r\n' + t('sfn.payment.successMessage.2'), 'success')
                    }}
                />
            </div>
        )
    }
}
