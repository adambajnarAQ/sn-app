import React from 'react'
import Configurator from '../../configurator'
import { i18n } from '../../validator'
import SingleCheckList from '../info-page/parts/single-check-list'
import Button from '../../front/components/shared/sn-button'
import SnIcon from '../../front/components/shared/sn-icon'
import { Link } from 'react-router-dom'

@i18n()
export default class PaymentModal extends Configurator {

    render() {
        const {
            t,
            closeModal,
        } = this.props

        return (
            <div className="payment-modal">
                <div className="d-flex flex-column align-items-center justify-content-center h-100">
                    <Button
                        className="position-absolute right-0 top-0 bg-transparent border-0"
                        aria-label="Close"
                        onClick={() => closeModal()}
                    >
                        <SnIcon className="sn-crusader sn-lg text-color-white" />
                    </Button>
                    <div className="d-flex flex-column align-items-center text-color-white mt-5">
                        <h1 className="text-color-white">{t('sfn.paymentModal.title')}</h1>
                        <p>{t('sfn.paymentModal.subtitle')}</p>

                        <Link
                            className="px-5 btn mt-4 btn-lg btn-circle text-white font-weight-extra-bold font-size-xl btn-sfn"
                            to="/sfn/payment">{t('sfn.paymentModal.buttonText')}
                        </Link>
                        <p className="mt-3">Only <b>1 USD per 1 month!</b></p>
                        <SingleCheckList
                            title={null}
                            containerClassName="bg-color-transparent mb-3"
                            checkListClassName="col-lg-12"
                            items={['Money back guarantee', '+3 months free', 'Much more...']}
                            bgTransparent
                        />
                    </div>
                </div>
            </div>
        )
    }
}
